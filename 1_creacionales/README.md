# Patrones de diseño creacionales
Su objetivo general es proporcionar mecanismos
de creación de objetos que
incrementan la flexibilidad y la
reutilización del código
existente.

Sus principales características son:
1. Se enfocan en las tarea más frecuente en las aplicaciones orientadas a objetos, la creación (ejemplificación) de los objetos.
2. Proporcionan mecanismos controlados, simples y uniformes para crear objetos.
3. Normalmente encapsulan y/u ocultan los detalles sobre las clases responsables de la creación de objetos.
4. Usan el concepto de interface para reducir el acoplamiento.

Se pueden encontrar los siguientes:
* Método factoría
* Fábrica abstracta
* Singleton
* Prototipo (*prototype*)
* Constructor (*builder*)
