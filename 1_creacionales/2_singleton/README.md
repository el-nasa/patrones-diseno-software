# Singleton
Se encarga de que solo haya una y solo una ejemplificación de una clase durante la vida de una aplicación. El patrón sugiere que la clase tiene que ser responsable ella misma de garantizar que solo exista una ejemplificación, los objetos cliente no pueden encargarse de esta tarea, por lo que las variables globales no son una solución para implementar el patrón.
Para poder implementarlo se puede:
* Hacer un constructor privado.
* Crear una interfaz pública en la forma de un método estático, dentro de esta se crea una sola vez la clase, y luego nada más es retornada. El método es estático para que los clientes puedan llamar al método sin necesidad de ejemplificar la clase.


## Ejercicios propuesto
Es una aplicación de interfaz gráfica que se encarga de registrar los archivos a un archivo de texto plano o a consola según la selección de un botón. El patrón es usado en el momento en el que se ejemplifica fileLogger o consoleLogger, ya que lo hacen una única vez, de ahí en adelante se retorna el objeto ya creado.

![modelo funcional 1](./img/modelo_funcional.png "modelo funcional")

![modelo estrctural 1](./img/modelo_estructural.png "modelo estrctural")


![modelo visual 2](./img/modelo_visual.png "modelo visual")
