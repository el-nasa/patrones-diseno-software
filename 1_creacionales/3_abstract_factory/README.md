# Factoría abstracta
Provee una interfaz para producir una familia de objetos. El patrón puede ser implementado en forma de interfaz o clase abstracta.
* Suites o familias de clases dependientes relacionadas.
* Un grupo de clases de una fábrica concreta que implementan la interfaz proporcionada.

Útil cuando un objeto cliente desea crear una ejemplificación de un conjunto de clases dependientes relacionadas sin saber que clase concreta específica se va a instanciar. 

## Ejercicio propuesto
Es una interfaz de selección de diferentes tipos de vehículos (autos o SUVs) para un concesionario, en donde los tipos de vehículo (familias) pueden ser de autos lujosos, semi lujosos o no lujosos. El patrón fue utilizado para poder crear las familias de clases similares según la elección del usuario sin tener que implementar la lógica necesaria para todas las posibles combinaciones de vehículos que se pueden crear  seleccionando una opción.

![modelo funcional](./img/modelo_funcional.png "modelo funcional")

![modelo estrctural](./img/modelo_estructural.png "modelo estrctural")

![modelo visual 2](./img/modelo_visual.png "modelo visual")
