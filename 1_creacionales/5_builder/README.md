# Constructor (*Builder*)
El patrón sugiere mover la lógica de construcción fuera de la clase objeto para que pudiera ser diseñada de forma más efectiva.
Puede haber más de un builder para una implementación diferente, resultando en una representación distinta del objeto.
El 

patrón permite:



* Que el diseño sea más modular con cada implementación contenida en un builder.

* Cada nueva implementación es más sencilla.
  
* El proceso de construcción de un objeto se vuelve independiente de los componentes que lo componen.

El patrón utiliza un director que se encarga  de invocar los diferentes métodos builder requeridos en la construcción (creación) de un objeto.

* El cliente invoca el método de construcción en la ejemplificación de director para comenzar el proceso de creación de objetos.
* El objeto cliente crea instancias de un implementador de constructores concreto apropiado.
* El cliente asocia al objeto Builder con el objeto director.


## Ejercicio propuesto
Se analiza una interfaz de usuario que modifica los elementos que muestra en pantalla según el tipo de sentencia SQL que se requiere obtener para buscar candidatos por número de años mínimos de experiencia y habilidad, por empleador y por empresa. El patrón Builder es utilizado para construir las diferentes interfaces en tiempo de ejecución, y se adecua bastante bien debido a que las vistas de las interfaces son objetos complejos que se componen de muchas partes. Al usar el patrón es sencillo implementar una nueva interfaz gráfica para extender la funcionalidad del programa.


![modelo funcional](./img/modelo_funcional.png "modelo funcional")

![modelo estrctural](./img/modelo_estructural.png "modelo estrctural")

![modelo dinamico deep](./img/modelo_visual.png "modelo dinamico deep")
