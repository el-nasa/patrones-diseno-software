# Prototipo (*prototype*)
Ofrece  una forma más flexible de creación de objetos que los patrones fábrica 
abstracta y método factoría. El patrón incluye  una alternativa a crear 
numerosas factorías, además cuando cliente necesita crear un conjunto de 
elementos que se parecen o difieren en su estado, el patrón ayuda a crear 
objetos costosos en términos de tiempo y procesamiento.
El patrón sugiere crear al objeto por adelantado y designarlo como objeto 
prototipo, y luego crear otros objetos haciendo copias con las 
modificaciones de sus estados. Existen dos tipos de copias:

* Copia profunda (*Deep Copy*): En esta copia los objetos de nivel 
  inferior que contiene al objeto de nivel superior también se duplican 
  y los objetos de nivel superior original y todos sus miembros primitivos 
  son duplicados.
  
* Copia superficial (*Shallow Copy*):  En esta copia los objetos de 
  nivel inferior que contiene al objeto de nivel superior no se duplican, 
  sólo se copian como referencias y los objetos de nivel superior original 
  y todos sus miembros primitivos son duplicados.



## Ejercicio propuesto
Es un sistema de simulación de creación de autos y personas cuya 
intencionalidad es mostrar las diferencias en los resultados por 
consola de la implementación de una copia profunda o una copia superficial. 
El patrón de diseño prototipo no se aprecia desde los modelos estructural 
o funcional, sino en el modelo dinámico. En la copia profunda se tiene un 
método *clone()* que al ser llamado crea un nuevo objeto de tipo persona 
con los valores de los estados del objeto original, en cambio en la copia 
superficial la clase que se quiere clonar implementa la interfaz *Clonable*  
y utiliza el método clone para realizar la copia.

![modelo funcional](./img/modelo_funcional.png "modelo funcional")

![modelo estrctural](./img/modelo_estructural.png "modelo estrctural")

![modelo dinamico deep](./img/modelo_dinamico_deep_copy.png "modelo dinamico deep")

![modelo dinamico shallow](./img/modelo_dinamico_deep_copy.png "modelo dinamico shallow")