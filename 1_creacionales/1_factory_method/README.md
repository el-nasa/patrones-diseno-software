# Método factoría
De forma general, todas las subclases en una jerarquía de clases heredan los métodos de su clase padre y  si se quiere otra funcionalidad se tiene que reescribir el método de creación.
El patrón soluciona escenarios en los que la aplicación necesita implementar un criterio de selección para instanciar la clase correcta manteniendo un bajo acoplamiento entre las clases de la aplicación.

El patrón consiste en definir un método en una clase que:

* Selecciona la clase correcta de una jerarquía de clases basado en un contexto y otros factores influyentes.
* Ejemplifica la clase adecuada y retorna un objeto del tipo de la clase padre, por lo que la aplicación cliente no necesita conocer los detalles de la jerarquía.

## Ejercicios propuestos
Durante el desarrollo del curso fueron propuestos dos ejercicios de modelado que sirvieron para refinar las habilidades de abstracción necesarias para realizar los siguientes modelos.

### Ejercicio 1
Es una aplicación por consola que se encarga de crear dos tipos diferentes de registradores (registro por consola y en archivo de texto plano) según el contexto, que en este caso está representado por la lectura de un valor de un archivo, y registrar el mensaje de la forma adecuada. La aplicación utiliza el método *getLogger()* para retornar el registrador seleccionado.

![modelo funcional 1](./img/modelo_funcional_1.png "modelo funcional")

![modelo estrctural 1](./img/modelo_estructural_1.png "modelo estrctural")
### Ejercicio 2
Es una aplicación de interfaz gráfica que se utiliza para registrar mensajes en diferentes tipos de salidas (archivo o consola). La particularidad de este ejercicio es que fue utilizado el lenguaje de programación python, por lo que se tuvieron que tener en cuenta las particularidades del lenguaje como la presencia de módulos cuyo modelado es distinto al de una clase tradicional.

![modelo funcional 2](./img/modelo_funcional_2.png "modelo funcional")

![modelo estrctural 2](./img/modelo_estructural_2.png "modelo estrctural")

![modelo visual 2](./img/modelo_visual_2.png "modelo visual")
