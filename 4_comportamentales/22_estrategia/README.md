# Estrategia
El patrón de estrategia es útil cuando hay un conjunto de algoritmos relacionados y un objeto cliente necesita poder elegir dinámicamente un algoritmo de este conjunto que se adapte a sus necesidades actuales.

El patrón de estrategia sugiere mantener la implementación de cada uno de los algoritmos en una clase separada. Cada uno de estos algoritmos encapsulados en una clase separada se denomina estrategia. Un objeto que usa un objeto de estrategia a menudo se denomina objeto de contexto.
Con diferentes objetos de estrategia en su lugar, cambiar el comportamiento de un objeto de contexto es simplemente una cuestión de cambiar su objeto de estrategia por el que implementa el algoritmo requerido.
Para permitir que un objeto de contexto acceda a diferentes objetos de estrategia de manera transparente, todos los objetos de estrategia deben estar diseñados para ofrecer la misma interfaz. En el lenguaje de programación Java, esto se puede lograr diseñando cada objeto de Estrategia como un implementador de una interfaz común o como una subclase de una clase abstracta común que declara la interfaz común requerida.

Una vez que el grupo de algoritmos relacionados se encapsula en un conjunto de clases de estrategia en una jerarquía de clases, un cliente puede elegir entre estos algoritmos seleccionando y creando una ejemplificación de una clase de estrategia adecuada. Para alterar el comportamiento del contexto, un objeto de cliente necesita configurar el contexto con la ejemplificación de la estrategia seleccionada. Este tipo de arreglo separa completamente la implementación de un algoritmo del contexto que lo usa. Como resultado, cuando se cambia la implementación de un algoritmo existente o se agrega un nuevo algoritmo al grupo, tanto el contexto como el objeto cliente (que usa el contexto) no se ven afectados.

## Estrategia vs otras alternativas
La implementación de diferentes algoritmos en forma de un método que utiliza declaraciones condicionales viola el principio básico de apertura y cierre orientado a objetos. Diseñar cada algoritmo como una clase diferente es un enfoque más elegante que diseñar todos los algoritmos diferentes como parte de un método en forma de declaración condicional. Debido a que cada algoritmo está contenido en una clase separada, resulta más simple y fácil agregar, cambiar o eliminar un algoritmo.

Otro enfoque sería hacer subclases del contexto en sí mismo e implementar diferentes algoritmos en las subclases del contexto. Este tipo de diseño vincula el comportamiento a una subclase de contexto y el comportamiento ejecutado por una subclase de contexto se vuelve estático. Con este diseño, para cambiar el comportamiento del contexto, un objeto cliente necesita crear una ejemplificación de una subclase diferente del contexto y reemplazar el objeto Contexto actual con él.

Tener diferentes algoritmos encapsulados en diferentes clases de estrategia desacopla el comportamiento del contexto del propio objeto Context. Con diferentes objetos de estrategia disponibles, un objeto de cliente puede usar el mismo objeto de contexto y cambiar su comportamiento configurándose con diferentes objetos de estrategia. Este es un enfoque más flexible que hacer subclases.

Además, a veces hacer subclases puede conducir a una jerarquía de clases inflada. Hemos visto un ejemplo de esto durante la discusión del patrón Decorator. El diseño de algoritmos como diferentes clases de estrategia mantiene lineal el crecimiento de la clase.

## Ejercicio propuesto
El ejercicio consiste en un sistema de registro de mensajes a archivo o consola
que puede utilizar diferentes algoritmos de cifrado cuando es establecido.
El patrón estrategia se puede ver que en que para cambiar el tipo de cifrado
usado solo es necesario llamar al método *setEncryptionStrategy(EncryptionStrategy)*
con el tipo de cifrado como argumento, el resto del protocolo de mensajes se sigue
manteniendo.