# Estado
El estado de un objeto se puede definir como su condición exacta en cualquier momento, dependiendo de los valores de sus propiedades o atributos. El conjunto de métodos implementados por una clase constituye el comportamiento de sus ejemplificaciones. Siempre que hay un cambio en los valores de sus atributos, decimos que el estado de un objeto ha cambiado.

El patrón de estado es útil para diseñar una estructura eficiente para una clase, una ejemplificación típica de la cual puede existir en muchos estados diferentes y exhibir un comportamiento diferente según el estado en el que se encuentra. En otras palabras, en el caso de un objeto de tal tipo clase, parte o todo su comportamiento está completamente influenciado por su estado actual. En la terminología de patrones de diseño de estado, dicha clase se conoce como clase de contexto. Un objeto de contexto puede alterar su comportamiento cuando hay un cambio en su estado interno y también se lo conoce como un objeto con estado.

## Ejercicio propuesto
Es un simulador de cuenta bancaría que utiliza el patrón estado para actualizar
el dinero despues de depositar o retirar dinero. El patrón estado se ve es que
para cada operación realizada, se consulta el estado previo para hacer el calculo
del dinero total, también se usa para determinar el siguiente estadó que tendrá la
cuenta, este puede ser *NoTransactionFreeState*, *OverDrawnState* y *TransactionFeeState*.

En el modelo estructural se puede ver en la asociación que tiene *BusinessAccount*
a *State* y viseversa. La cuenta puede guardar uno de los estados de la jerarquía
de la clase *State*.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')
![modelo funcional](./img/modelo_visual.png 'modelo visual')