# Observador
El patrón observador es útil para diseñar un sistema de comunicación consistente entre un conjunto dependiente de objetos y un objeto del que son dependientes. Esto le permite a los objetos dependientes tener sus estados sincronizados con el objeto que son dependientes. El conjunto de objetos dependientes son llamados observadores y el objeto del que dependen es llamado sujeto (subject). Para lograr esto, el patrón sugiere un modelo editor-suscriptor  que conduzca a un límite claro entre el conjunto de objetos Observer y el objeto Sujeto.

Un observador típico es un objeto con interés o dependencia en el estado del sujeto. Un sujeto puede tener más de un observador. Cada uno de estos observadores necesita saber cuándo el sujeto sufre un cambio en su estado.

El sujeto no puede mantener una lista estática de tales observadores, ya que la lista de observadores de un sujeto dado podría cambiar dinámicamente. Por tanto, cualquier objeto con interés en el estado del sujeto debe registrarse explícitamente como un observador con el sujeto.
Siempre que el sujeto sufre un cambio en su estado, notifica a todos sus observadores registrados. Al recibir la notificación del sujeto, cada uno de los observadores pregunta al sujeto para sincronizar su estado con el del sujeto. Por lo tanto, un sujeto se comporta como un editor al publicar mensajes para todos sus observadores suscritos.

Para que el mecanismo funcione:
* El subject debe proporcionar una interfaz para registrarse y cancelar el registro de notificaciones de cambios.
* Alguna de las siguientes se debe cumplir:
    * modelo in the pull: El sujeto debe proporcionar una interfaz que permita a los observadores consultarlo y obtener la información de estado requerida para actualizar su estado.
    * modelo in the push: El sujeto debe enviar la información de su estado a los observadores que puedan estar interesados.
* Los observadores deben proporcionar una interfaz para recibir notificaciones del sujeto.

### Añadir nuevos observadores
Después de aplicar el patrón Observer, se pueden agregar diferentes observadores dinámicamente sin requerir ningún cambio en la clase Subject. De manera similar, los observadores no se ven afectados cuando cambia la lógica de cambio de estado del sujeto.

## Ejercicio propuesto
Es una interfaz gráfica para administrar las ventas de una tienda, esta tiene tres partes
la primera llamada *ReportManager* sirve para seleccionar el departamento que se quiere ver,
una vez es seleccionado alguno las otras dos ventanas actualizan la información recuperada
de un archivo plano *Transactions.dat*. El patrón observador es utilizado por 
estas dos ventas que muestran la información de las ventas de la tienda según el departamento
seleccionado, estas son suscritas al subject desde que son ejemplificadas (se les pasa el subject como
parámetro para que puedan usar el método register()), y cuando el actionListener es llamado
se ejecuta el método *notify* que va por cada uno de los observadores suscritos
y actualiza la información, lo que lo vuelve un observador de modelo *push*.

![modelo funcional](./img/modelo_funcional.png)
![modelo estructural](./img/modelo_estructural.png)