# Método plantilla
El patrón de método de plantilla es uno de los patrones de diseño más simples y más utilizados en aplicaciones orientadas a objetos.

El patrón del método de plantilla se puede utilizar en situaciones en las que existe un algoritmo, algunos pasos del cual se pueden implementar de múltiples formas diferentes. En tales escenarios, el patrón del Método de plantilla sugiere mantener el esquema del algoritmo en un método separado denominado método de plantilla dentro de una clase, que puede denominarse clase de plantilla, dejando fuera las implementaciones específicas de las porciones variantes (pasos que se puede implementar de múltiples formas diferentes) del algoritmo a diferentes subclases de esta clase.
La clase Plantilla no necesariamente tiene que dejar la implementación a las subclases en su totalidad. En cambio, como parte de proporcionar el esquema del algoritmo, la clase Plantilla también puede proporcionar cierta cantidad de implementación que puede considerarse invariante en diferentes implementaciones. Incluso puede proporcionar una implementación predeterminada para las partes variantes, si corresponde. Solo se implementarán detalles específicos dentro de diferentes subclases. Este tipo de implementación elimina la necesidad de código duplicado, lo que significa que se debe escribir una cantidad mínima de código.
Utilizando el lenguaje de programación Java, la clase Template se puede diseñar de una de las dos formas siguientes.

## Clase abstracta
Este diseño es más adecuado cuando la clase Plantilla proporciona solo el esquema del algoritmo sin ninguna implementación predeterminada para sus partes variantes. Suponiendo que los diferentes pasos del algoritmo se pueden convertir en métodos individuales:
* El método Plantilla puede ser un método no abstracto concreto con llamadas a otros métodos que representan diferentes pasos del algoritmo.
* La clase Plantilla puede implementar partes invariantes del algoritmo como un conjunto de métodos no abstractos.
* El conjunto de pasos variantes se puede diseñar como métodos abstractos. Se pueden proporcionar implementaciones específicas para estos métodos abstractos dentro de un conjunto de subclases concretas de la clase Plantilla abstracta.

En este diseño, la clase abstracta declara métodos y cada una de las subclases implementa estos métodos de una manera específica sin alterar el esquema del algoritmo.

## Clase concreta

Este diseño es más adecuado cuando la clase Plantilla proporciona, además del esquema del algoritmo, la implementación predeterminada para sus partes variantes. Suponiendo que los diferentes pasos del algoritmo se pueden convertir en métodos individuales:

* El método plantilla puede ser un método no abstracto concreto con llamadas a otros métodos que representan diferentes pasos del algoritmo.
* La clase Plantilla puede implementar partes invariantes del algoritmo como un conjunto de métodos no abstractos.
* El conjunto de pasos variantes se puede diseñar como métodos no abstractos con la implementación predeterminada. Las subclases de la clase plantilla pueden anular estos métodos para proporcionar implementaciones específicas sin alterar el esquema del algoritmo.

De ambas estrategias de diseño, se puede ver que la implementación del patrón de plantilla se basa en gran medida en la herencia y la anulación de funciones. Por lo tanto, siempre que se use la herencia para implementar los detalles, se puede decir que el patrón de método de plantilla se usa en su forma más simple.