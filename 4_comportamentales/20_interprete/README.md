# Interpretador
En general, los lenguajes están hechos de un conjunto de reglas gramaticales. Se pueden construir diferentes oraciones siguiendo estas reglas gramaticales. A veces, una aplicación puede necesitar procesar ocurrencias repetidas de solicitudes similares que son una combinación de un conjunto de reglas gramaticales. Estas solicitudes son distintas pero similares en el sentido de que todas se componen utilizando el mismo conjunto de reglas. Un ejemplo simple de este tipo sería el conjunto de diferentes expresiones aritméticas enviadas a un programa de calculadora. Aunque cada una de estas expresiones es diferente, todas se construyen utilizando las reglas básicas que componen la gramática del lenguaje de las expresiones aritméticas.

En tales casos, en lugar de tratar cada combinación distinta de reglas como un caso separado, puede ser beneficioso que la aplicación tenga la capacidad de interpretar una combinación genérica de reglas. El patrón intérprete se puede utilizar para diseñar esta capacidad en una aplicación de modo que otras aplicaciones y usuarios puedan especificar operaciones utilizando un lenguaje simple definido por un conjunto de reglas gramaticales.

Aplicación del patrón de intérprete:
* Se puede diseñar una jerarquía de clases para representar el conjunto de reglas gramaticales con cada clase en la jerarquía representando una regla gramatical separada.
* Se puede diseñar un módulo del intérprete para interpretar las oraciones construidas utilizando la jerarquía de clases diseñada anteriormente y realizar las operaciones necesarias.

Debido a que una clase diferente representa cada regla gramatical, el número de clases aumenta con el número de reglas gramaticales. Un idioma con reglas gramaticales extensas y complejas requiere una gran cantidad de clases. El patrón de intérprete funciona mejor cuando la gramática es simple. Tener una gramática simple evita la necesidad de tener muchas clases correspondientes al complejo conjunto de reglas involucradas, que son difíciles de administrar y mantener.

## Ejercicio propuesto
El ejercicio es una calculadora que toma como entrada una expresión de una
cadena de texto. El patrón interprete se ve en que se representa el conjunto
de reglas aritméticas como una jerarquía de la interfaz *Expresion*, y el 
programa es capaz de interpretar, realizar un árbol sintáctico y finalmente
evaluar la expresión dada.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')