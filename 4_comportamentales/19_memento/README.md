# Memento
El estado de un objeto puede ser definido como los valores de las propiedades o atributos de este en cualquier punto del tiempo. El patrón memento es útil para diseñar mecanismos para capturar y almacenar el estado de cualquier objeto para que subsecuentemente, cuando sea necesario, el objeto pueda ser devuelto a su anterior estado. Esto es simular la operación de deshacer. El patrón memento puede ser usado para alcanzar esto sin exponer la estructura interna del objeto. El objeto cuyo estado necesita ser capturado es llamado originador. Cuando un cliente quiere guardar el estado del originador solicita el estado actual del originador. El originador almacena todos sus atributos que son requeridos para restaurar el estado en un objeto separada llamado memento que es retornado al cliente. Entonces un memento puede ser visto como un objeto que contiene el estado interno de otro objeto en un momento del tiempo. Un objeto memento debe ocultar los valores de las variables del originador  de todos los objetos excepto el originador. Para lograrlo, el memento debe ser diseñado para proveer acceso restringido a otros objetos mientras que se mantiene el acceso al originador.

Cuando un cliente quiere restaurar el originador a su estado previo, simplemente pasa el memento hacia el originador. El originador usa la información del estado contenida en el memento y la coloca de vuelta en sí misma usando los valores almacenadas en el objeto memento.

La clase memento está definida como una clase dentro de otra más grande (similar a flyweight), de esta forma el objeto que la contiene puede acceder a los métodos de memento sin que otras clases lo puedan hacer.


## Ejercicio propuesto
Es un sistema convertidor de registros a sentencias SQL, este se encarga de recuperar los nombres de diferentes clientes de un archivo de texto plano llamado “Data.txt” a otro con las sentencias SQL adecuadas para ingresar la información a una base de datos, este segundo archivo se llama SQL.txt. El patrón de diseño memento es utilizado para evitar que cuando el programa se encuentre haciendo la conversión se encuentre con un registro dañado, pierda el progreso y vuelva a iniciar. En este caso el memento guarda el ultimo elemento que pudo ser convertido y marca un error en pantalla, que una vez es corregido recupera ese estado y sigue desde donde se detuvo.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')
