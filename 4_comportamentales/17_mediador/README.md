# Mediador
En general, las aplicaciones orientadas a objetos se componen de un conjunto de objetos que interactúan unos con los otros con el propósito de proveer un servicio. Esta interacción puede ser directa (punto a punto) siempre que el número de objetos referenciados los unos con los otros sea muy baja. 

Cuando el número de objetos aumenta, este tipo de interacción directa puede llevar a complejizar las referencias entre los objetos, lo que puede afectar a la mantenibilidad de la aplicación. El tener objetos directamente referenciados a otros reduce significativamente la capacidad  de reuso de estos objetos por su alto nivel de acoplamiento.

En estos caso, el patrón mediador puede ser usado para diseñar un modelo de comunicación controlado y coordinado de un grupo de objetos, eliminando la necesidad de tener objetos directamente conectados.
El patrón mediado sugiere abstraer todos los detalles de las interacciones de los objetos en una clase separada. Cada objeto en el grupo es responsable por ofrecer el servicio diseñado para este fin, pero los objetos no interactúan de forma directa unos con otros. La interacción entre cualquier dos objetos diferentes es dirigida a través de la clase mediadora. Todos los objetos y sus mensajes van hacia el mediador, que envía los mensajes a los objetos apropiados según los requerimientos de la aplicación. El diseño resultante tiene las siguientes ventajas:
* Con todas las interacciones de los objetos trasladadas a un objeto separado (mediador), se vuelve más fácil el alterar el comportamiento las interrelaciones de los objetos, al reemplazar el mediador con alguna de sus subclases con funcionalidad extendida o alterada.
* Al mover las dependencias entre objetos de los objetos individuales resulta en mayor reusabilidad de los objetos.
* Dado que los objetos no necesitan referirse unos a los otros directamente, a los objetos se les pueden realizar pruebas unitarias más fácilmente.
* El resultado de un bajo grado de acoplamiento permite a las clases individuales ser modificadas sin afectar a otras clases.

## Mediador vs Fachada

|Mediador|Fachada|
|-----|-----|
|Es usado para abstraer la funcionalidad necesaria de un grupo de objetos con el propósito de simplificar la interacción de objetos|Usado para abstraer la funcionalidad de un subsistema de componentes, con el propósito de proveer una interfaz de alto nivel simplificada|
|Todos los objetos interactúan unos con los otros mediante el mediador. El grupo de objetos conoce la existencia del mediador|Los clientes de la fachada interactúan con los componentes del subsistema. La existencia de la fachada no es conocida por los componentes del subsistema.|
|Dado que el mediador y todos los objetos son registrados dentro pueden comunicarse unos con los otros, la comunicación es bidireccional|El cliente envía mensajes por medio de la fachada hacia el subsistema, pero no viceversa, haciendo la comunicación unidireccional.|
|El mediador puede asumirse que está en la mitad de un grupo de objetos que interactúan. Al usar mediador le permite a la implementación de cualquier objetos que interactúan ser cambiados sin tener impacto en otras interacciones|La fachada se sienta entre un objeto cliente y un subsistema. Al usar fachada le permite a la implementación de un subsistema el ser cambiado completamente sin tener impacto sobre los clientes.|
|Al hacer subclases del mediador, el comportamiento de las interrelaciones puede ser extendida|Al hacer subclases de la fachada, la implementación de la interfaz de alto nivel puede ser cambiada.| 

## Ejercicio propuesto
Es un simulador de servidor FTP, que utiliza el patrón mediador en conjunto al patrón comando para cuando sea llamado un comando la funcionalidad sea redirigida al mediador y ahí se lleve la lógica de negocio.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')
![modelo estructural](./img/modelo_visual.png 'modelo visual')

