# Comando
De forma tradicional una aplicación envía una solicitud al conjunto de objetos Receptores que ofrecen los servicios para procesar la solicitud están unidos unos a otros tal que su interacción es directa. Esto puede llevar a que se requieran más sentencias if para añadir una nueva funcionalidad, por lo que se violaría el concepto de abierto/cerrado de la programación orientada a objetos.

Al usar el patrón Comando, el invocador envía una solicitud en nombre del cliente y el conjunto de service-rendering de los objetos receptores puede ser desacoplado. El patrón sugiere que al crear una abstracción para que el procesamiento o la acción pueda ser llevada en respuesta a las solicitudes de los clientes.

Un objeto comando dado es responsable de ofrecer la funcionalidad requerida para el procesamiento de la solicitud que representa, pero no contiene la implementación de la funcionalidad. Los objetos comando hacen uso de los objetos receptores para ofrecer la funcionalidad.

Cuando una aplicación cliente necesita ofrecer un servicio en respuesta a la interacción de un usuario (u aplicación):

1. Crear los objetos receptores necesarios
2. Crea el comando apropiado y lo configura con los objetos receptores del paso 1
3. Ejemplifica al invocador y lo configura con el objeto Comando del paso 2
4. El invocador invoca el método execute() del objeto comando.
5. Como parte de la implementación del método execute, un típico objeto comando invoca los métodos necesarios en los objetos receptores que contiene para proveer el servicio requerido.

En este acercamiento de diseño se tiene:
* El cliente/invocador no tiene interacción directa con los objetos receptores y por tanto, hay un desacoplamiento completo de cada uno.
* Cuando una aplicación necesita ofrecer una nueva funcionalidad, un nuevo objeto comando es añadido. Esto no requiere ningún cambio en el código del invocador. Por esta razón el patrón conserva el principio de abierto cerrado.
* Dado que la solicitud está diseñada en forma de objeto, abre una nueva cantidad de posibilidades como:
    * Guardar el objeto comando en medios persistentes para:
        * Ser ejecutada después
        * Para aplicar procesamiento en reversa para la funcionalidad de deshacer operaciones.
    * Agrupar diferentes objetos comandos para ser ejecutados como una sola unidad.

## Ejercicio propuesto
Es un simulador de servidor FTP, en el que se “pueden” subir, bajar y eliminar archivos remotas y locales con operaciones de mover elementos de las listas de un lado a otro o eliminar elementos de las listas. El patrón comando se puede ver en el actionperformed, que de forma tradicional mezcla la lógica de programación de la interfaz con las acciones de los objetos, pero al usar el patrón comando se pueden encapsular estas distintas lógicas que son llamadas con el método actionperformed.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')
![modelo estructural](./img/modelo_visual.png 'modelo visual')
