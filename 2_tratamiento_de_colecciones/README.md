# Patrones de tratamiento de colecciones
Los patrones de tratamiento de colecciones son técnicas y modelos que buscan facilitar la usabilidad y extensibilidad del código que hace uso de  múltiples objetos en tiempo de ejecución (también llamadas colecciones de objetos).

Los patrones de esta categoría son:
* Tratamiento uniforme de objetos compuestos (*Composite*).
* Iterador
* De información intrínseca y extrínseca de objetos (*Flyweight*).
* Visitador.
