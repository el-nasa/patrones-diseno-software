# Tratamiento uniforme de objetos compuestos (Composite)

Útil para diseñar interfaces comunes para componentes individuales o compuestos para programas clientes puedan ver estos dos tipos de manera uniforme. El protocolo de mensajería para los objetos simples y compuestos es el mismo, ya que se utiliza la misma firma de métodos

En términos de árboles permite referenciación uniforme para los nodos terminales no (colecciones) y los nodos terminales (unidades).

Existen dos tipos de componentes: **componentes compuestos y componentes individuales**. Es importante que cada nuevo método de una clase hija tiene que ser añadida al padre para que siga funcionando el patrón.

Existen dos enfoques para implementar el patrón:

* Enfoque 1: impuro porque delega a la máquina virtual ver como se trata
  ![enfoque 1](./img/enfoque1.PNG 'enfoque 1')

* Enfoque 2: Se utiliza una clase abstracta en la que se definen la excepciones, es más puro porque se puede ver en tiempo de diseño
  ![enfoque 2](./img/enfoque2.PNG 'enfoque 2')
## Ejercicio propuesto
Es un simulador de un sistema de archivo de un sistema operativo Windows o Unix. Este programa crea la estructura básica de un sistema de archivos simple compuesto por directorios y archivos anidados, y el programa es capaz de ir por cada uno de los elementos recuperando el valor del tamaño para luego sumarlas y obtener el valor del sistema de archivos general.
El patrón de diseño se puede observar en el modelo estructural como la asociación que va de la clase directorio hacia la clase *FileSystemPart*, esta relación indica que un directorio es capaz de almacenar múltiples objetos de tipo *FileSystemPart* dentro de un vector.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estrcutural')