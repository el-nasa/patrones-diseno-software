# Iterador
Permite al objeto cliente acceder a los contenidos de un contenedor de manera secuencial, sin tener que saber nada sobre la representación interna de los contenidos. El patrón utiliza el concepto de Contenedor que se interpreta como una colección de datos u objetos. Existen los siguientes tipos de iteradores:

### Internos
la colección ofrece otros métodos para permitir que un cliente visite diferentes objetos dentro de la colección. Se caracterizan por:

* Solo puede haber un iterador en una colección en un momento dado.
* La colección debe mantener o guardar el estado de iteración.
* La clase java.util.Resultset contiene los datos y también ofrece los métodos como next para navegar la lista de elementos.
* Utilizar si afectar el estado de la colección original, bloquearla no me afecta.

### Filtrados o externos
En el caso de java.util.vector, su iterador solo devuelve el siguiente elemento dentro de una colección. Se puede implementar comportamientos más complejos. La funcionalidad de iteración se separa de la colección y se mantiene dentro de un objeto diferente denominado iterador.

Por ejemplo un iterador que puede devolver un conjunto seleccionado de objetos (en lugar de todos los objetos) en un orden secuencial. En este tipo de iteradores:

* La sobrecarga implicada en el almacenamiento del estado de iteración no está asociada con la colección. Se encuentra en el objeto iterador exclusivo.
* Puede haber varios iteradores en una colección determinada en un momento dado.
* Adicionalmente la clase java.util.vector tiene su iterador definido en forma de un objeto separado de tipo enumeración. Este objeto se devuelve a un objeto cliente en respuesta a la llamada al método elements().
* Utilizar si interesa afectar el estado de la colección original o modificarla.

## Ejercicio propuesto

Es una aplicación gráfica que se encarga de devolver a los candidatos a un puesto de trabajo de un archivo de texto plano que cumplen ciertos requisitos. Este ejercicio se presentó con pequeñas variaciones dependiendo del tipo de iterador usado, en el caso del iterador interno esta aplicación solo devuelve la lista de candidatos y en el iterador externo devuelve los candidatos según un criterio de filtrado.

![modelo estructural](./img/modelo_visual_externo.png 'modelo visual')

### modelos iterador externo
![modelo funcional](./img/modelo_funcional_externo.png 'modelo funcional externo')
![modelo estructural](./img/modelo_estructural_externo.png 'modelo estrcutural externo')

### modelos iterador interno

![modelo funcional](./img/modelo_funcional_interno.png 'modelo funcional interno')
![modelo estructural](./img/modelo_estructural_interno.png 'modelo estrcutural interno')
