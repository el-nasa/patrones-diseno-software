# Visitor
Es un patrón útil para diseñar una operación en una colección heterogénea de objetos de una jerarquía de clases. Permite definir una operación sin cambiar ninguna de las clases de alguno de los objetos en la colección.
Se sugiere definir la operación en una clase visitor aparte tal que se separa la operación de la colección. Para cada nueva operación se crea una nueva clase visitor.

### Idea de diseño 1
Puede diseñarse cada visitador para implementar una interfaz visitor Interface correspondiente, una interfaz típica declara un conjunto de métodos de visita, uno para cada tipo de objeto en la colección de objetos.
```
visit(classA objclassA) // para procesar objetos clase A

visit(Class B, objClass B)

```


### Idea de diseño 2
Clases de objetos de la colección necesitan definir el método accept (visitor). El cliente interesado en ejecutar la operación visitor necesita:

* Crear la colección de objetos e invocar el método accept(visitor) en cada miembro de la colección pasando la instancia del visitador creada en el paso anterior.
* Crear una ejemplificación de implementer(visitor) de la interfaz visitorInterface que ya está diseñado para soportar la operación requerida.


## Diseño en Java
* La clase flyweight está diseñada con un constructor privado que evita que los clientes ejemplifican al constructor.
* Un singleton supone que solo se crea una ejemplificación de sí mismo, pero la naturaleza es de nivel de tipo de clase, sin embargo en el flyweight mantiene una ejemplificación de sí misma por cada tipo de flyweight del sistema. Estos objetos flyweight son almacenados en la primera variable estática flyweight, lo que puede ser visto como una variable del singleton.
* Cuando un cliente necesita crear un flyweight invoca al método estático getflyweight pasando el tipo requerido de flyweight como argumento.
* La principal ventaja de usar este patrón es que La app cliente puede obtener ahorros considerables en uso de memoria y tiempo.
* Los objetos cliente se les debe prohibir ejemplificar al flyweight directamente, al mismo tiempo los objetos clientes deben tener una forma de acceder a determinado flyweight cuando es necesario.

### Añadiendo objetos de un nuevo tipo de colección
* Cada objeto de la colección hace un llamado a su respectivo visit(objectType) pasándose a si mismo como argumento.
* La clase del objeto debe proveer un método similar a accept(visitor) y como parte de la implementación de método debe invocar el método visit(objetType) en el objeto visitador pasándose a sí mismo como argumento.
* Un método visit(objectType) necesita ser añadido a la interfaz visitorInterface y necesita ser implementado por todas las clases visitor concretas.

## Ejercicio propuesto
Es un sistema de Liquidación de órdenes con GUI, este sistema se encarga de calcular el precio de órdenes hechas en California, Ohio o sobre el mar sumandole valores como el costo e impuestos adicionales.  El patrón visitador se ve en el caso de uso de crear orden, en donde una vez es recuperado el tipo de orden, es ejemplificada una variable visitor de forma polimórfica, la cual es enviada como parámetro en el método accept de los diferentes tipos de órdenes, de esta forma la funcionalidad de recuperar el precio de la orden es definida de forma externa a las clases.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')

![modelo estructural](./img/modelo_estructural.png 'modelo estrcutural')

![modelo visual](./img/modelo_visual.png 'modelo visual')

