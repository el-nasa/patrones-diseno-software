# De información intrínseca y extrínseca de objetos (Flyweight)

Sugiere separar todos los datos comunes intrínsecos en un objeto denominado flyweight object. El grupo de elementos puede compartir el objeto flyweight ya que representa su estado intrínseco, lo que elimina la necesidad de almacenar la misma información intrínseca invariante del objeto.

Solo existe un objeto de tipo flyweight dado que es compartido con todos los otros objetos apropiados.

El patrón establece que se pueden separar la información en dos categorías:

* Información extrínseca: Dependiente y variable, en diversos contextos para objetos ejemplificados desde una misma clase.
* Información intrínseca: independiente del contexto del objeto, es decir invariante o constante entre diferentes ejemplares de una clase dada.

#### Enfoque de diseño sin factoría
![modelo estructural](./img/enfoque_dis_sin_factoria.png 'modelo estrcutural interno')

#### Enfoque de diseño con factoría
![modelo estructural](./img/enfoque_dis_con_factoria.png 'modelo estrcutural interno')

## Diseño en Java
* La clase flyweight está diseñada con un constructor privado que evita que los clientes ejemplifican al constructor.
* Un singleton supone que solo se crea una ejemplificación de sí mismo, pero la naturaleza es de nivel de tipo de clase, sin embargo en el flyweight mantiene una ejemplificación de sí misma por cada tipo de flyweight del sistema. Estos objetos flyweight son almacenados en la primera variable estática flyweight, lo que puede ser visto como una variable del singleton.
* Cuando un cliente necesita crear un flyweight invoca al método estático getflyweight pasando el tipo requerido de flyweight como argumento.
* La principal ventaja de usar este patrón es que La app cliente puede obtener ahorros considerables en uso de memoria y tiempo.
* Los objetos cliente se les debe prohibir ejemplificar al flyweight directamente, al mismo tiempo los objetos clientes deben tener una forma de acceder a determinado flyweight cuando es necesario.

### Segunda estrategia
La segunda estrategia de implementación consiste en separar la responsabilidad de crear y mantener diferentes singleton en una factoría flyweightfactory. La clase flyweight puede ser diseñada como una clase dentro de flyweightfactory.
En esta forma las ejemplificaciones de flyweight son usadas sólo para representar el estado intrínseco de un objeto.

## Ejercicio propuesto
Es un sistema de generación de plantilla de texto para carnets estudiantiles que coge la información extrínseca de cada estudiante y la formatea usando los datos genéticos (intrínsecos) requeridos para hacer un carnet estudiantil. Existen dos acercamientos en este ejercicio, en la versión 1 la clase FlywerightTest utiliza un objeto de la clase IdCard donde se guarda de forma temporal la información de cada estudiante y luego es reemplazada por la del siguiente estudiante en la colección, en la versión 2 la clase  flyweight es la que despliega la información.

![modelo funcional](./img/modelo_funcional_v1.png 'modelo funcional v1')

### Versión 1
![modelo estructural v1](./img/modelo_estructural_v1.png 'modelo estrcutural v1')

### Versión 2
![modelo estructural v2](./img/modelo_estructural_v2.png 'modelo estrcutural v2')

