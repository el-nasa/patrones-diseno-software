# Patrones estructurales
Los patrones de diseño estrcuturales sirven de forma general
para aumentar la funcionalidad del software en tiempo de diseño (quizás
es de ahi viene su nombre).
Los patrones son:
* Decorador.
* Adaptador.
* Cadena de responsabilidad.
* Fachada.
* Puente (*Bridge*).
* *Proxy* (Existen varios tipos, pero en el curso fueron abordados el
  proxy contable y el virtual).