# Adaptador

De forma general, los clientes de una clase acceden a los servicios de una clase por medio de su interfaz. Algunas veces una clase puede proporcionar la funcionalidad deseada pero su interfaz no es la esperada. Esto puede ocurrir por varias razones como que la interfaz existente es muy detallada o muy poco, o puede que la terminología utilizada en la interfaz sea diferente de lo que el cliente está buscando.

En estos casos es necesario que la interfaz sea convertida en otra interfaz que el cliente espera, preservando la usabilidad de la clase existente. Sin esta conversión, el cliente no podría usar la funcionalidad ofrecida por la clase. Esto se puede lograr al usar el patrón adaptador.

El patrón adaptador sugiere definir un objeto envolvente (wrapper) conocido como adaptador que envuelve al objeto adaptado (adaptee). El adaptador provee la interfaz requerida esperada por el cliente. La implementación de una interfaz adaptador convierte la solicitud del cliente en una llamada a un método del adaptador, que internamente llama al método de la clase que adapta. Esto le permite al cliente tener un acceso indirecto a la clase adaptee. Entonces, un adaptador puede ser usado para hacer que clases trabajen juntas que de otra manera no podrían hacerlo por tener interfaces incompatibles.

El término interfaz usado en la discusión:

* No se refiere al concepto de interfaz en el lenguaje de programación Java, aunque la interfaz de unas clases puede ser declarada usando una interfaz en Java.
* No se refiere a la interfaz de usuario GUI
* Se refiere a la interfaz de programación que la clase expone, que es usada por otras clases. Por ejemplo, cuando una clase es diseñada como una clase abstracta o una interfaz de Java, el conjunto de métodos declarados dentro constituyen la interfaz de la clase.

## Adaptadores de clase
Diseñada haciendo *subclassing* de la clase *adaptee*. Además, un adaptador de clase implementa la interfaz esperada por el objeto cliente. Cuando un objeto cliente invoca un método de adaptador, el adaptador internamente llama al método que heredó.

## Adaptadores de objeto
Contiene una referencia al objeto adaptado. Similar al adaptador de clase, un adaptador de objeto también implementa la interfaz que el cliente espera. Cuando el objeto del cliente llama a un método de adaptador de objeto, el adaptador de objeto invoca al método apropiado en la ejemplificación del *adaptee* cuya referencia contiene.

|Adaptador de clase|Adaptador de objeto|
|----------------|---------|
|Basado en el concepto de herencia|Usa composición de objetos|
|Puede ser usado para adaptar únicamente la interfaz de adaptee. No puede adaptar las interfaces de sus subclases, como el adaptador está estáticamente enlazado con el adaptee cuando es creado.|Puede ser usado para adaptar la interfaz de adaptee y todas sus subclases.|No puede sobrescribir los métodos de adaptee. 
Nota: Literalmente no puede sobrescribir porque no hay herencia. Pero las funciones del wrapper proveída por el adaptador puede cambiar su comportamiento cuando sea requerido.|Dado que adaptee está diseñado como una subclase de adaptee, es posible sobreescribir algo del comportamiento de adaptee. Nota: En java, una subclase no puede sobrescribir un método declarado como final en la clase padre.|
|El cliente tiene algo de conocimiento de la interfaz de adaptee así como de toda la interfaz pública de adaptee.|El cliente y el adaptee está completamente desacoplados. Solo el adaptador sabe la interfaz de adaptee.|
|En aplicaciones Java: Es conveniente cuando la interfaz esperada está disponible en forma de una interfaz de java y no como una clase abstracta o concreta. Esto se debe a que en Java solo está permitida la herencia única. Como un adaptador de clase está diseñado como una subclase de la clase adaptada, no podrá ser una subclase de la clase interfaz (que representa la interfaz esperada) y, si la interfaz esperada está disponible en forma de clase abstracta o concreta.| En aplicaciones Java: Es aconsejable cuando la interfaz que un objeto cliente espera está disponible en forma de clase abstracta. También puede ser usada si la interfaz esperada está disponible en forma de un interfaz de Java o cuando existe la necesidad de adaptar la interfaz del adaptee y también todas sus subclases.|
|En aplicaciones Java: Puede adaptar métodos con el especificador de acceso protegido|En aplicaciones Java: No puede adaptar métodos con el especificador de acceso protegido, a no que el adaptador esté diseñado para ser parte del mismo paquete.|

## Ejercicio propuesto
Se presenta la situación problemática en la que un interfaz encargado de la recolección de datos de una oficina postal que presta su servicios para USA y Canadá no puede usar directamente la interfaz de CAAadress por su firma de método incompatible con la interfaz AddressValidator. Siendo más concretos la firma del método AddressValidator es isValidAddress, pero la de CAAddress es isValidadCanadianAddr. 

### Adaptador de clase
En este acercamiento se propone una clase adaptadora llamada CAAddressAdapter que hereda de CAAdress y a su vez es una realización de la interfaz AddressValidator. Cuando el método isValidadAddress es llamado en el adaptador este retorna el resultado de isValidadCanadianAddr que se visualiza como un paso de mensajes a sí mismo porque CAAddressAdapter hereda de CAAdress, por lo que puede usar sus métodos.
![modelo funcional](./img/modelo_funcional_a_clase.png 'modelo funcional')

![modelo funcional](./img/modelo_estructural_a_clase.png 'modelo estructural')

### Adaptador de objeto
A diferencia del anterior acercamiento, ya no se utiliza una interfaz AddressValidator, ahora es una clase abstracta, por tal motivo no es posible tomar el acercamiento de adaptador de clase porque en java no está permitido hacer herencia múltiple.
Similarmente se propone una clase llamada CAAaddressAdapteeer que hereda de la clase abstracta AddressValidator, esta tiene una asociación con CAAaddress (en vez de heredar de ella) y esta es la principal diferencia entre los acercamientos. En este caso no se pueden sobrescribir los métodos de CAAddress.
Cuando el customer hace el llamado, ejemplifica a CAAaddress y a CAAddressAdapter, y a este último le pasa como argumento CAAddress durante su ejemplificación, finalmente el resultado se lo pide a CAAddressAdapter. Cuando es solicitado isValidadAddress éste se lo pide al dapatador que internamente llama al método isValidadCanadianAddr al objeto internamente referenciado.

![modelo funcional](./img/modelo_funcional_a_objeto.png 'modelo funcional')

![modelo funcional](./img/modelo_estructural_a_objeto.png 'modelo estructural')





