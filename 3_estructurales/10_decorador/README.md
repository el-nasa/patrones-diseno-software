# Decorador
El patrón decorador es usado para extender la funcionalidad de un objeto de forma dinámica sin tener que cambiar la clase fuente original o haciendo uso de herencia. Esto es logrado al crear un objeto wrapper al que se refiere como decorador que envuelve el objeto actual.

## Características 
* El objeto decorador está diseñado para tener la misma interfaz que el objeto que envuelve. Esto le permite al cliente  interactuar con el objeto decorador de la misma forma que lo haría con el objeto actual.
* El objeto decorador contiene una referencia al objeto actual.
* El objeto decorador recibe todas las peticiones (llamados) del cliente. Estas son redirigidas al objeto envuelto.
* El objeto decorador añade alguna funcionalidad adicional antes o después de redirigir la petición al objeto envuelto. Esto asegura que la funcionalidad adicional sea añadida al objeto de forma externa en tiempo de ejecución sin tener que modificar su estructura.

Típicamente, en el diseño orientado a objetos, la funcionalidad de una clase es extendida usando herencia, la siguiente tabla lista las diferencias entre el patrón decorador y el uso de herencia:

|Patrón decorador|Herencia|
|----------------|---------|
|Usado para extender la funcionalidad de un objeto particular|Usado para extender la funcionalidad de una clase de objetos|
|No requiere de subclases|Requiere de subclases
|Dinámico|Estático|
|Asignación de responsabilidades en tiempo de ejecución|Asignación de responsabilidades en tiempo de compilación o diseño|
|Previene la proliferación de subclases que lleva a un diseño con menor complejidad y menor confusión|Puede llevar al diseño de numerosas subclases, ampliando bastante una jerarquía de clases en casos específicos|
|Más flexible|Menos flexible|
|Es posible tener múltiples objetos decoradores para un objeto dado de forma simultánea. Un cliente puede escoger qué capacidades quiere al enviar mensajes al decorador apropiado| Al tener subclases para todas las posibles combinaciones con las capacidades adicionales que un objeto cliente podría esperar lleva a la proliferación de subclases|
|Es fácil añadir cualquier combinación de capacidades. La misma capacidad puede ser incluso añadida dos veces|Difícil|


## Añadir un nuevo decorador
1. Cree una clase de utilidad FileReader con un método para leer líneas de un archivo.

2. EncryptLogger en la aplicación de ejemplo cifra un texto dado desplazando los caracteres a la derecha en una posición. Cree un DecoratorDecryptFileReader para que FileReader agregue la función de descifrado, después de leer los datos de un archivo.

3. Mejore la clase DecoratorClient para hacer lo siguiente: - Escriba un mensaje en un archivo usando EncryptLogger. - Lea usando el decorador DecryptFileReader para mostrar el mensaje en una forma no encriptada.

## Ideas de diseño
1. Debe existir un wrapper que es el objeto que envuelve a otro para decorarlo
2. Se debe heredar lo que se quiere decorar
3. Se hace la ampliación a nivel de decoración añadiendo atributos o nuevos métodos.

# Ejercicio propuesto
El ejercicio consiste en un programa simulador de creación de figuras geométricas con varias características. Se aplica el patrón decorador para evitar una explosión de clases que vendría de diseñar todas las posibles subclases de figuras que se podrían ejemplificar.

Las dos figuras principales son circle y rectangle, la cuales se les quiere añadir características adicionales como el color, grosor y estilo de línea, para ello se crea una nueva clase realizadora de Shape llamada ShapeDecorator de la cual heredan las diferentes nuevas funcionalidades que van a envolver a las figuras Circle o Rectangle. La clase abstracta ShapeDecorator tiene una asociación a Shape, de esta forma puede realizar el paso de mensajes a otra clase que sea de tipo Shape.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')

![modelo estructural](./img/modelo_estructural.png 'modelo estructural')

