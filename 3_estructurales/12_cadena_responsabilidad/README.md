# Cadena de responsabilidad
El patrón cadena de responsabilidad (CoR) recomienda una bajo grado de acoplamiento entre un objeto que envía una petición y un conjunto potencial de objetos receptores de solicitudes.
Cuando hay más de un objeto que puede gestionar o satisfacer una solicitud de un cliente, el patrón CoR recomienda dar a cada uno de estos objetos la oportunidad de procesar la solicitud en un orden secuencial. Al aplicar el patrón CoR en estos casos, cada uno de esos posibles objetos manejadores pueden ser organizados en la forma de una cadena, cada objeto apuntando al siguiente objeto de la cadena.
El primer objeto en la cadena recibe la petición y decide si la puede atender o si la pasa al siguiente objeto en la cadena. La petición pasa por todos los objetos en la cadena uno tras del otro hasta que la solicitud es atendida por uno de ellos o la petición llega al final sin ser procesada.
Por ejemplo, si A B C son objetos capaces de procesar a petición, en este orden, A debería atender la petición o pasarla a B sin determinar si B puede atenderla. Al recibir la solicitud, B deberia atenderla o pasarla a C. Cuando C recibe la petición, deberá atenderla o acabar el paso de mensaje sin que la petición sea procesada. En otras palabras, una petición enviada a la cadena de receptores puede no ser atendida incluso si ha llegado al final de la cadena.

Estas son algunas de las características importantes del patrón CoR:
* Un conjunto de receptores de objeto potenciales y el orden que se encuentran en forma de cadena pueden ser determinados dinámicamente en tiempo de ejecución por el cliente dependiendo en el estado actual de la aplicación.
* Un cliente puede tener diferentes conjuntos de objetos manejadores para diferentes tipos de petición dependiendo su estado actual. También, un objeto receptor dado puede necesitar pasar una petición entrante a otros tipos diferentes de objetos receptores dependiente en el tipo de petición y el estado de la aplicación cliente. Para que estas comunicaciones sean simples, todos los posibles objetos receptores deben proveer una interfaz consistente. En java puede lograrse al tener diferentes objetos receptores que implementen una interfaz en común o sean subclases de una clase abstracta padre.

# Ejercicio propuesto
Es una aplicación por consola que simula el flujo por el que pasa una solicitud de autorización de préstamo de una compañía, en la que si se supera 50000 en la primera solicitud al BranchManager este redirige la solicitud al regionalDirector cuyo monto máximo es mayor, y así hasta llegar al presidente de la compañía. Si el valor de la compra es mayor a su límite, la solicitud se resuelve con un mensaje que dice que debido al alto costo de la solicitud se tiene que revisar en la mesa directiva.

El flujo de la autorización es creado por la clase cliente PRManager en el método createAuthorizationFlow().

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')

![modelo estructural](./img/modelo_estructural.png 'modelo estructural')

