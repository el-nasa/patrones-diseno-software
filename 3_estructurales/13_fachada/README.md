# Fachada
El patrón fachada se ocupa de un subsistema de clases. Un subsistema es un conjunto de clases que trabajan juntas unas con otras con el propósito de proveer un conjunto de características relacionadas (funcionalidad). Por ejemplo, la clase Account, la clase Address y la clase CreditCard trabajan juntas, como parte de un subsistema, para proveer características para un cliente en línea.
En las aplicaciones del mundo real, un subsistema podría consistir en un gran número de clases. Los clientes de un subsistema pueden necesitar interactuar con número de clases del subsistema para sus necesidades. Este tipo de interacción directa entre objetos cliente y el subsistema lleva a un alto grado de acoplamiento entre los objetos cliente y el subsistema. Cuando una clase del subsistema tiene un cambio, como un cambio en su interfaz, todas las clases cliente dependientes pueden verse afectadas.

El patrón fachada es util en este tipo de situaciones. El patrón provee una interfaz simplificada y de alto nivel para un subsistema, resultado en una reducción de complejidad y dependencia. Esto hace que el uso del subsistema sea más sencillo y manejable.

Una clase fachada provee una interfaz simplificada para un subsistema que es utilizada por unos clientes. Con el objeto fachada en su lugar, los clientes interactúan con el objeto fachada en vez
de hacerlo directamente con las clases del subsistema. El objeto fachada toma responsabilidad de interactuar con las clases de un subsistema. En efecto, los clientes interactúan con la fachada para tratar con el subsistema. Entonces el patrón fachada promueve un bajo acoplamiento entre un subsistema y sus clientes.
![ejemplo no fachada](./img/ejemplo_no_fachada.PNG 'ejemplo sin fachada')

De la anterior figura se puede ver que el objeto fachada desacopla y protege a los clientes de los objetos del subsistema. Cuando un subsistema tiene un cambio, los clientes no se ven afectados como antes.
![ejemplo fachada](./img/ejemplo_fachada.PNG 'ejemplo con fachada')

Aunque los clientes usan una interfaz simplificada proveída por la fachada, cuando es necesario, un cliente tendrá acceso a los componentes de un subsistema directamente mediante interfaces de bajo nivel del subsistema como si el objeto fachado no existiera. En este caso, se tendría el mismo problema de dependencia/acoplamiento de antes.

## Notas importantes
Algunas consideraciones importantes al aplicar el patrón fachada:
* Una fachada no puede ser diseñada para proveer ninguna funcionalidad adicional.
* Nunca se pueden retornar componentes del subsistema desde los métodos de la fachada hacia los clientes. Por ejemplo el método
  CreditCard getCreditCard() expondría el subsistema a los clientes y la aplicación puede no ser capaz de obtener todos los beneficios de usar el patrón fachada.
* El objetivo de la fachada es proporcionar una interfaz de alto nivel y por lo tanto, preferiblemente un método de la fachada debería ofrecer un servicio comercial de alto nivel en vez de desempeñar una tarea individual de bajo nivel.

## Ejercicio propuesto
El ejercicio es una aplicación con interfaz gráfica que se encarga de la validación y guardado en archivo plano de unas cuentas de tarjeta de crédito (Visa, Master y Discover). Se utiliza el patrón fachada para evitar que la clase cliente, en este caso ButtonHandler, no tenga que interactuar de forma directa con las clases que se encargan las diferentes validaciones (Account, CreditCard y Address) sino que interactúe directamente con una sola clase llama CustomerFacade. Esta clase ejecuta la funcionalidad de validación cuando es llamado el método saveCustomerData(), el cual internamente llama a los métodos de validación de las clases del subsistema.
En cuanto al diseño del modelo estructural se puede ver el uso del patrón en la clase CustomerFacade que tiene asociaciones a las clases del subsistema, y de esta forma las crea llama a los métodos que son necesarios. 

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')

![modelo estructural](./img/modelo_estructural.png 'modelo estructural')

![modelo visual](./img/modelo_visual.png 'modelo visual')

  

