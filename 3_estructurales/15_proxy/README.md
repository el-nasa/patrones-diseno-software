# Counting Proxy
El patrón proxy contable es usado para diseñar un conjunto de operaciones adicionales como el registro y conteo que necesitan ocurrir antes y/o después de que un objeto cliente invoque un método en un objeto proveedor de servicio. En vez de mantener estas operaciones adicionales dentro del objeto proveedor de servicio, el patrón proxy contable sugiere encapsular la funcionalidad adicional en un objeto separado llamado “counting proxy”. Una de las característica que un objeto bien diseñado es que ofrece una funcionalidad bien definida, en otras palabras, un objeto, idealmente, no debería hacer varias cosas que no están relacionadas.
El encapsular las funcionalidades de registro, conteo y otras similares dentro de un objeto separado deja que el objeto proveedor de servicio con una funcionalidad bien definida.

Un proxy contable está diseñado para tener la misma interfaz que el objeto proveedor de servicios al que un objeto cliente accede. En vez de acceder al objeto proveedor de servicio directamente, el objeto cliente invoca los métodos del proxy contable. El proxy realiza el conteo y registro y redirige la llamada al método al objeto proveedor de servicio.

## Ejercicio de clase
Es una aplicación que se encarga de contar el número de veces y el tiempo que se tarda en abrir y cargar un archivo con unas órdenes (que no se saben de que sean). EL patrón se puede ver en el modelo estructural en la clase OrderProxy que es una realización de la interfaz OrderIF (así mantiene los métodos) que es ejemplificada antes que la clase Order para realizar toda la lógica de contar el número de veces que se abre el archivo y cuanto tiempo tarda en abrirse y cargarse. Está lógica es aplicada cuando se llama el método getAllOrders en el OrderProxy. En este ejemplo la clase cliente no ejemplifica a la clase con la funcionalidad real.
![modelo funcional](./img/modelo_funcional_counting.png 'modelo funcional')

![modelo estructural](./img/modelo_estructural_counting.png 'modelo estructural')

#Proxy virtual
El patrón proxy virtual es una técnica para reducir el uso de memoria que recomienda posponer la creación de un objeto hasta que sea necesaria: si cuando se crea dicho objeto produce un alto costo en computación en términos de almacenamiento o procesamiento. En una aplicación típica, diferentes objetos hacen partes de diferentes partes de la funcionalidad. Cuando una aplicación es iniciada, puede no necesitar de todos los objetos disponibles de forma inmediata. En esos casos, el patrón proxy virtual sugiere aplazar la creación de esos objetos hasta que sea necesario para la aplicación. El objeto es creado por primera vez como una referencia en la aplicación y la misma ejemplificación es reusada desde ese punto en adelante.

##Ventajas
Hace que la aplicación inicie más rápido, ya que no necesita crear y cargar todos los objetos de la aplicación.

##Desventajas
Dado que no hay garantía de que un objeto de la aplicación sea creado, en toda parte donde el objeto sea accedido es necesario verificar que no sea null, lo que provoca una penalización en tiempo gastado en la verificación.


Al aplicar el patrón, un objeto separado llamado proxy virtual puede ser diseñado para que su interfaz sea la misma a la del objeto actual. Diferentes objetos clientes pueden crear ejemplificaciones del correspondiente proxy virtual y usarlo en lugar del objeto actual. Cuando un cliente llama al proxy virtual mantiene una referencia al objeto actual como una variables ejemplificadas. El proxy no crea de forma automática el objeto actual. Cuando un cliente invoca el método en el proxy virtual que requiere de los servicios del objeto actual, verifica si ese objeto ya ha sido creado.

* Si el objeto actual ya ha sido creado, el proxy redirige el mensaje al objeto actual
* Si el objeto actual no ha sido creado:
* * Crea el objeto actual
* * Asigna el objeto actual a la variable referenciada
* * Redirige la llamada al objeto actual

Con esta alineación, los detalles como la existencia actual de un objeto y el redireccionamiento de los mensajes son ocultados a los objetos cliente. Como resultado, los objetos cliente son libres de verificar si existe el objeto actual. También, como el tiempo de procesamiento es menor al crear un virtual proxy que al crear el objeto actual, el proxy virtual puede ser ejemplificado al comienzo de la aplicación cliente en lugar del objeto actual.

## Ejercicio de clase
Es una aplicación que simula algunas de las funciones de un IDE (compilar, ejecutar y generar documentación). El patrón virtual proxy es utilizado porque la operación de generar documentación, que requiere de la ejemplificación de RealProcessor es “pesada” en términos de computación (almacenamiento y/o procesamiento) por lo que pospone su ejemplificación hasta que realmente es necesario, es decir cuando hace el llamado con el método generateDocs.
El patrón se puede ver en el modelo estructural en que se declara una clase ProxyProcessor la cual hereda de la clase abstracta IDEOperation y que mantiene una asociación con la clase RealProcessor. El objeto cliente corre la funcionalidad llamando los métodos heredados de virtual proxy que no requieren de mucho “poder de cómputo”, pero cuando llega la parte de generar la documentación el objeto proxy verifica si ya está creado el objeto real, si no lo está lo crea y luego le redirige el mensaje para que este haga la operación (todo esto se hace así al hacer la reescritura del método en el proxy, si no es sobreescrito entonces ejecuta la operación definida en la clase abstracta). 

![modelo funcional](./img/modelo_funcional_virtual.png 'modelo funcional')

![modelo estructural](./img/modelo_estructural_virtual.png 'modelo estructural')
